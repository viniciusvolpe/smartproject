package br.com.quick.smartproject.tests;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import br.com.quick.smartproject.entities.Costumer;
import br.com.quick.smartproject.entities.Phone;
import br.com.quick.smartproject.entities.User;
import br.com.quick.smartproject.query.builder.QueryBuilder;

public class QueryBuilderTest {
	
	private QueryBuilder queryBuilder;
	
	@Before
	public void creatQueyBuilder(){
		queryBuilder = new QueryBuilder();
	}
	
	@Test
	public void createQueyWithSimplesAttributes() throws Exception{
		User user = new User("viniciusvolpe", "123");
		String query = queryBuilder.createQueryWithoutJoin(user);
		Assert.assertEquals("from br.com.quick.smartproject.entities.User where 1 = 1 and login = ? and password = ? ", query);
	}
	
	@Test
	public void createQueryWithCompositionAttributes() throws Exception{
		Costumer costumer = new Costumer("Vinicius", new User("viniciusvolpe", "123"), new ArrayList<Phone>());
		String query = queryBuilder.createJoinQuery(costumer);
		System.out.println(query);
		Assert.assertEquals("from br.com.quick.smartproject.entities.Costumer costumer join costumer.user where 1 = 1 and name = ? ", query);
	}
}
