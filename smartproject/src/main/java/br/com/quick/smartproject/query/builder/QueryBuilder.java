package br.com.quick.smartproject.query.builder;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import br.com.quick.smartproject.entities.Costumer;
import br.com.quick.smartproject.entities.User;
import br.com.quick.smartproject.entity.Entity;

public class QueryBuilder {

	private static final String COMPARISON_STATEMENT = " = ? ";
	private static final String AND_KEYWORD = "and ";
	private static final String DEFAULT_WHERE_CLAUSE = " where 1 = 1 ";
	private static final String FROM_KEYWORD = "from ";

	public String createQueryWithoutJoin(Entity entity) throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		StringBuilder query = new StringBuilder();
		query.append(FROM_KEYWORD + entity.getClass().getName());
		query.append(DEFAULT_WHERE_CLAUSE);
		for(Field field : entity.getFields()){
			if(entity.invokeGet(field.getName()) instanceof Entity) continue;
			if(entity.invokeGet(field.getName()) instanceof Collection) continue;
			Object fieldValue = entity.invokeGet(field.getName());
			if(fieldValue != null ){
				query.append(AND_KEYWORD + field.getName() + COMPARISON_STATEMENT);
			}
		}
		return query.toString();
	}

	public String createJoinQuery(Entity entity) throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		StringBuilder query = new StringBuilder();
		String splittedName[] = entity.getClass().getName().split("\\.");
		String alias = splittedName[splittedName.length - 1].toLowerCase();
		query.append(FROM_KEYWORD + entity.getClass().getName()+" "+alias);
		for(Field field : entity.getFields()){
			if(entity.invokeGet(field.getName()) instanceof Entity){
				String splitterFieldName[] = field.getGenericType().getTypeName().split("\\.");
				String fieldAlias = splitterFieldName[splitterFieldName.length - 1].toLowerCase();
				query.append(" join " + alias + "." + fieldAlias);
			}
		}
		query.append(DEFAULT_WHERE_CLAUSE);
		for (Field field : entity.getFields()) {
			if(entity.invokeGet(field.getName()) instanceof Entity) continue;
			if(entity.invokeGet(field.getName()) instanceof Collection) continue;
			Object fieldValue = entity.invokeGet(field.getName());
			if(fieldValue != null ){
				query.append(AND_KEYWORD + field.getName() + COMPARISON_STATEMENT);
			}
		}
		return query.toString();
	}
	
	
}
