package br.com.quick.smartproject.entity;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

public abstract class Entity {
	
	private static final String GET_PREFIX = "get";
	private static final String SET_PREFIX = "set";

	public List<Field> getFields(){
		return Arrays.asList(this.getClass().getDeclaredFields());
	}
	
	public Field getField(String name) throws NoSuchFieldException, SecurityException{
		return this.getClass().getField(name);
	}
	
	public List<Annotation> getAnnotations(){
		return Arrays.asList(this.getClass().getAnnotations());
	}
	
	public Object invokeGet(String field) throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException{
		Method methodGet = this.getClass().getDeclaredMethod(GET_PREFIX + StringUtils.capitalize(field));
		return methodGet.invoke(this);
	}
	
	public void invokeSet(String field, Object param) throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException{
		Method methodSet = this.getClass().getDeclaredMethod(SET_PREFIX + StringUtils.capitalize(field), param.getClass());
		methodSet.invoke(this, param);
	}
}
	
